﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Data;
using ObjectDetection.DataStructures;
using ObjectDetection.YoloParser;

namespace ObjectDetection
{
    class OnnxModelScorer
    {
        private readonly string imagesFolder;
        private readonly string modelLocation;
        private readonly MLContext mlContext;

        private IList<YoloBoundingBox> _boundingBoxes = new List<YoloBoundingBox>();

        public OnnxModelScorer(string imagesFolder, string modelLocation, MLContext mlContext)
        {
            this.imagesFolder = imagesFolder;
            this.modelLocation = modelLocation;
            this.mlContext = mlContext;
        }

        public struct ImageNetSettings
        {
            public const int imageHeight = 416;
            public const int imageWidth = 416;
        }

        public struct TinyYoloModelSettings
        {
            // for checking Tiny yolo2 Model input and  output  parameter names,
            //you can use tools like Netron, 
            // which is installed by Visual Studio AI Tools

            // input tensor name
            public const string ModelInput = "image";

            // output tensor name
            public const string ModelOutput = "grid";
        }

        private ITransformer LoadModel(string modelLocation)
        {
            Console.WriteLine("Read model");
            Console.WriteLine($"Model location: {modelLocation}");
            Console.WriteLine($"Default parameters: image size=({ImageNetSettings.imageWidth},{ImageNetSettings.imageHeight})");

            // Create IDataView from empty list to obtain input data schema
            var data = mlContext.Data.LoadFromEnumerable(new List<ImageNetData>());

            //             DataViewSchema predictionPipelineSchema;
            //             ITransformer model = mlContext.Model.Load(modelLocation, out predictionPipelineSchema  

            var tfmodel = mlContext.Model.LoadTensorFlowModel(modelLocation);
            var schema = tfmodel.GetModelSchema();
            var inputSchema = tfmodel.GetInputSchema();

            var pipeline = mlContext.Transforms.LoadImages(outputColumnName: "DecodeJPGInput", imageFolder: "", inputColumnName: nameof(ImageNetData.ImagePath))
                           .Append(mlContext.Transforms.Conversion.MapValueToKey(inputColumnName: "Label", outputColumnName: "LabelAsKey"))
                           .Append(mlContext.Transforms.ResizeImages(outputColumnName: "DecodeJPGInput", imageWidth: ImageNetSettings.imageWidth, imageHeight: ImageNetSettings.imageHeight, inputColumnName: "DecodeJPGInput"))
                           .Append(mlContext.Transforms.ExtractPixels(outputColumnName: "DecodeJPGInput"))
                           .Append(mlContext.Model.LoadTensorFlowModel(modelLocation).ScoreTensorFlowModel(outputColumnNames: new[] { "Score" }, inputColumnNames: new[] { "DecodeJPGInput" }, addBatchDimensionInput: true));

            // Fit scoring pipeline
            var model = pipeline.Fit(data);

            return model;
        }

        private IEnumerable<float[]> PredictDataUsingModel(IDataView testData, ITransformer model)
        {
            Console.WriteLine($"Images location: {imagesFolder}");
            Console.WriteLine("");
            Console.WriteLine("=====Identify the objects in the images=====");
            Console.WriteLine("");

            var debug = testData.Preview();

            IDataView scoredData = model.Transform(testData);

            //IEnumerable<float[]> probabilities = scoredData.GetColumn<float[]>(TinyYoloModelSettings.ModelOutput);
            IEnumerable<float[]> probabilities = scoredData.GetColumn<float[]>("Score");

            return probabilities;
        }

        public IEnumerable<float[]> Score(IDataView data)
        {
            //var tempData = PreData(data);
            var model = LoadModel(modelLocation);

            return PredictDataUsingModel(data, model);
        }
    }
}

