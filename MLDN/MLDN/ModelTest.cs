﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.ML.Vision;
using MLDN;
using ImageProcessor;
using ImageProcessor.Processors;
using Microsoft.ML;
using Microsoft.ML.Data;
using System.Drawing.Imaging;
using System.IO;

//4-40

// 8->26
// 27->41
// 41->69
// 68->86
// 86->106

public class TrainModelInput
{
    [ColumnName("Label"), LoadColumn(0)]
    public float Label { get; set; }


    [ColumnName("ImageSource"), LoadColumn(1)]
    public string ImageSource { get; set; }
}

public class TrainModelInput_Type
{
    [ColumnName("Label"), LoadColumn(0)]
    public string Label { get; set; }


    [ColumnName("ImageSource"), LoadColumn(1)]
    public string ImageSource { get; set; }
}

public class TrainModelOutput
{
    // ColumnName attribute is used to change the column name from
    // its default value, which is the name of the field.
    [ColumnName("PredictedLabel")]
    public Single Prediction { get; set; }
    public float[] Score { get; set; }
}

public class TrainModelOutput_Type
{
    // ColumnName attribute is used to change the column name from
    // its default value, which is the name of the field.
    [ColumnName("PredictedLabel")]
    public String Prediction { get; set; }
    public float[] Score { get; set; }
}



class ModelTest
{
    public static Form1 uiController = null;

    public static List<Image> GetSplitImg(Image img_ , string type_ = "type0")
    {
        if (type_ == "Type0")
        {
            return GetSplitImg_0(img_);
        }
        else if (type_ == "Type1")
        {
            return GetSplitImg_1(img_);
        }
        else if (type_ == "Type2")
        {
            return GetSplitImg_2(img_);
        }
        else if (type_ == "Type3")
        {
            return GetSplitImg_3(img_);
        }
        else
        {
            return GetSplitImg_4(img_);
        }    
    }

    public static List<Image> GetSplitImg_0(Image img_)
    {
        List<Image> listRet = new List<Image>();
        ImageFactory imageFactory = new ImageFactory(preserveExifData: true);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(8, 4, 23, 36)).Image);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(25, 4, 25, 36)).Image);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(45, 4, 27, 38)).Image);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(70, 4, 20, 36)).Image);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(87, 4, 22, 36)).Image);

        return listRet;
    }

    public static List<Image> GetSplitImg_1(Image img_)
    {
        List<Image> listRet = new List<Image>();
        ImageFactory imageFactory = new ImageFactory(preserveExifData: true);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(8, 4, 23, 36)).Image);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(25, 4, 22, 36)).Image);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(42, 4, 29, 38)).Image);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(66, 4, 22, 36)).Image);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(81, 4, 26, 36)).Image);

        return listRet;
    }

    public static List<Image> GetSplitImg_2(Image img_)
    {
        List<Image> listRet = new List<Image>();
        ImageFactory imageFactory = new ImageFactory(preserveExifData: true);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(8, 4, 23, 36)).Image);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(25, 4, 21, 36)).Image);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(41, 4, 27, 38)).Image);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(63, 4, 20, 36)).Image);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(78, 4, 28, 36)).Image);

        return listRet;
    }

    public static List<Image> GetSplitImg_3(Image img_)
    {
        List<Image> listRet = new List<Image>();
        ImageFactory imageFactory = new ImageFactory(preserveExifData: true);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(8, 4, 23, 36)).Image);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(25, 4, 24, 36)).Image);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(44, 4, 29, 38)).Image);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(66, 4, 22, 36)).Image);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(83, 4, 26, 36)).Image);

        return listRet;
    }

    public static List<Image> GetSplitImg_4(Image img_)
    {
        List<Image> listRet = new List<Image>();
        ImageFactory imageFactory = new ImageFactory(preserveExifData: true);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(8, 4, 21, 36)).Image);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(23, 4, 24, 36)).Image);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(41, 4, 28, 38)).Image);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(64, 4, 20, 36)).Image);
        imageFactory.Load(img_);
        listRet.Add(imageFactory.Crop(new Rectangle(80, 4, 24, 36)).Image);

        return listRet;
    }


    static MLContext mlContext = new MLContext(seed: 1);
    static ITransformer mlModel = null;
    static ITransformer mlModel_Type = null;
    public static void LoadModel()
    {
        string modelName = "MLModel.zip";

        DataViewSchema schema;
        mlModel = mlContext.Model.Load(modelName, out schema);

        uiController.PrintLog($"{modelName} Load Complete");
        uiController.PrintLog($"schema : {schema.ToString()}");

        string typeModelName = "MLModel_type.zip";
        mlModel_Type = mlContext.Model.Load(typeModelName, out schema);

        uiController.PrintLog($"{typeModelName} Load Complete");
        uiController.PrintLog($"schema : {schema.ToString()}");

        //         using(FileStream fStream = new FileStream("mlmodel.onnx", FileMode.OpenOrCreate))
        //         {
        //             IEnumerable<TrainModelInput> images = Directory.GetFiles("TempImg").Select(filePath => new TrainModelInput { ImageSource = filePath, Label = -1 });
        //             var dataView = mlContext.Data.LoadFromEnumerable(images);
        //             mlContext.Model.ConvertToOnnx(mlModel, dataView, fStream);
        //         }


    }

    public static List<string> AnalyzeImg(IList<Image> listImg_)
    {
        for(int i=0; i<listImg_.Count; ++i)
        {
            listImg_[i].Save($"TempImg\\Re{i}.png", ImageFormat.Png);
        }

        List<string> listRet = new List<string>();
        IEnumerable<TrainModelInput> images = Directory.GetFiles("TempImg").Select(filePath => new TrainModelInput { ImageSource = filePath ,Label = -1 });
        var predEngine = mlContext.Model.CreatePredictionEngine<TrainModelInput, TrainModelOutput>(mlModel);
        foreach (var item in images)
        {
            var ret = predEngine.Predict(item);
            listRet.Add(ret.Prediction.ToString());
        }


        
        return listRet;
    }

    public static string AnalyzeImgType(string path_)
    {
        TrainModelInput_Type input = new TrainModelInput_Type();
        input.ImageSource = path_;
        input.Label = "Nothing";
        var predEngine = mlContext.Model.CreatePredictionEngine<TrainModelInput_Type, TrainModelOutput_Type>(mlModel_Type);

        var ret = predEngine.Predict(input);
        return ret.Prediction.ToString();
    }
}

