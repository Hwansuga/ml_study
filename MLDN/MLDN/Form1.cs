﻿using ImageProcessor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace MLDN
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void PrintLog(string msg_)
        {
            this.listBox_log.Invoke(new MethodInvoker(delegate () {
                this.listBox_log.Items.Add(msg_);
            }));
        }

        private void button_Capter1_Click(object sender, EventArgs e)
        {
            PrintLog("분석 시작!");
            Task task = new Task(() => {
                Chapter1.Do();
            });
            task.Start();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Chapter1.uiController = this;
            ModelTest.uiController = this;

            ModelTest.LoadModel();
        }

        private void button_CaptureImage_Click(object sender, EventArgs e)
        {
            
        }

        Image targetImg = null;
        private void button_ModelTest_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Images Files(*.jpg; *.jpeg; *.gif; *.bmp; *.png)|*.jpg;*.jpeg;*.gif;*.bmp;*.png";
            DialogResult dr = ofd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                targetImg = new Bitmap(ofd.FileName);

                Task task = new Task(() =>
                {
                    string ret = ModelTest.AnalyzeImgType(ofd.FileName);
                    label_Type.Invoke(new MethodInvoker(delegate ()
                    {
                        label_Type.Text = ret;
                    }));

                    var listImg = ModelTest.GetSplitImg(targetImg, "Type1");

                    pictureBox_LoadImg.Image = targetImg;
                    pictureBox1.Image = listImg[0];
                    pictureBox2.Image = listImg[1];
                    pictureBox3.Image = listImg[2];
                    pictureBox4.Image = listImg[3];
                    pictureBox5.Image = listImg[4];

                    PrintLog($"Type : {ret} 분석 완료");
                });

                PrintLog("Type 분석 시작");
                task.Start();

                
            }
            else
            {

            }
        }

        private void button_Analyze_Click(object sender, EventArgs e)
        {
            var listImg = ModelTest.GetSplitImg(targetImg);

            PrintLog("분석 시작");
            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            Task task = new Task(() =>
            {
                var ret = ModelTest.AnalyzeImg(listImg);
                sw.Stop();

                string values = string.Join(",", ret.ToArray());


                label_Result.Invoke(new MethodInvoker(delegate ()
                {
                    label_Result.Text = values;
                }));

                PrintLog($"{sw.ElapsedMilliseconds} 분석 완료");
            });
            task.Start();

            

        }

        private void button_split_Click(object sender, EventArgs e)
        {
            PrintLog("작업 시작");
            Task task = new Task(() =>
            {
                var files = Directory.GetFiles("ComTEst", "*", searchOption: SearchOption.AllDirectories);
                foreach (var file in files)
                {
                    if ((Path.GetExtension(file) != ".jpg") && (Path.GetExtension(file) != ".png"))
                        continue;

                    Image temp = new Bitmap(file);
                    List<Image> listRet = ModelTest.GetSplitImg(temp);

                    var label = Path.GetFileName(file);

                    for (int i = 0; i < listRet.Count; ++i)
                    {
                        listRet[i].Save($"TempImg\\{label}_{i}.png", ImageFormat.Png);
                    }

                    PrintLog($"{label} is done");

                }

                PrintLog("작업 완료");
            });
            task.Start();
        }

        private void button_AnalyzeAll_Click(object sender, EventArgs e)
        {
            
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    string[] files = Directory.GetFiles(fbd.SelectedPath);

                    PrintLog("작업 시작");
                    Task task = new Task(() =>
                    {
                        int co = 0;
                        int wr = 0;
                        foreach (var file in files)
                        {
                            if ((Path.GetExtension(file) != ".jpg") && (Path.GetExtension(file) != ".png"))
                                continue;

                            Image temp = new Bitmap(file);
                            var label = Path.GetFileName(file);

                            string type = ModelTest.AnalyzeImgType(file);

                            List<Image> listRet = ModelTest.GetSplitImg(temp, type);
                            var ret = ModelTest.AnalyzeImg(listRet);
                            string fullString = string.Join("", ret.ToArray());
                            
                            if (label.Contains(fullString))
                            {
                                //PrintLog("Correct!");
                                co++;
                            }
                            else
                            {
                                PrintLog($"{label} , type : ${type} , result : {fullString} -- wrong");
                                var arrRet = fullString.ToCharArray();
                                var arrLabel = label.ToCharArray();
                                for (int i=0; i< arrRet.Length; ++i)
                                {
                                    if (arrRet[i] != arrLabel[i])
                                    {
                                        listRet[i].Save($"WrongImg\\{label.Replace(".jpg","")}_{arrRet[i]}.png");
                                    }
                                }
                                //System.IO.File.Copy(file, $"WrongImg\\{label}");
                                wr++;
                            }
                            
                        }

                        PrintLog($"작업 완료  {co} / {wr}");
                    });
                    task.Start();

                }
            }
        }

        private void button_Split_Save_Path_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    PrintLog($"Source Path {fbd.SelectedPath}");

                    var projectDirectory = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, "../../../"));
                    var assetsRelativePath = Path.Combine(projectDirectory, "assets\\NumberImg2");

                    string[] files = Directory.GetFiles(fbd.SelectedPath, "*.*", SearchOption.AllDirectories);
                    PrintLog("분리 작업 시작");
                    Task task = new Task(()=> {
                        foreach (var file in files)
                        {
                            if ((Path.GetExtension(file) != ".jpg") && (Path.GetExtension(file) != ".png"))
                                continue;
                            Bitmap temp = new Bitmap(file);
                            var label = Path.GetFileName(file).Replace(".jpg", "").Replace(".png", "");
                            var labelName = Path.GetFileName(Path.GetDirectoryName(file));


                            //                             OpenCvSharp.Mat scr = OpenCvSharp.Extensions.BitmapConverter.ToMat(temp);
                            //                             OpenCvSharp.Mat binary = new OpenCvSharp.Mat();
                            // 
                            //                             OpenCvSharp.Cv2.CvtColor(scr, binary, OpenCvSharp.ColorConversionCodes.BGR2GRAY);
                            //                             OpenCvSharp.Cv2.Threshold(scr, binary, 50, 255, OpenCvSharp.ThresholdTypes.Binary);

                            //                             ImageFactory factory = new ImageFactory();
                            //                             factory.Load(OpenCvSharp.Extensions.BitmapConverter.ToBitmap(binary));
                            //                             factory.GaussianBlur(7);
                            //                             factory.Contrast(100);
                            //                             factory.GaussianSharpen(20);
                            //                             binary = OpenCvSharp.Extensions.BitmapConverter.ToMat((Bitmap)factory.Image);
                            //                             OpenCvSharp.Cv2.FastNlMeansDenoising(binary, binary, 74, 6, 22);

                            //var type = ModelTest.AnalyzeImgType(file);

                            PrintLog($"{label} work , type : {labelName}");

                            List<Image> listRet = ModelTest.GetSplitImg(temp, labelName);
                            var arrLabel = label.ToCharArray();

                            for(int i=0; i<listRet.Count; ++i)
                            {
                                listRet[i].Save($"{assetsRelativePath}\\{arrLabel[i]}\\{label}_{i}.png");
                            }


                        }
                        PrintLog("분리 작업 완료");
                    });
                    task.Start();
                }
            }
        }

        private void button_Threshold_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    string[] files = Directory.GetFiles(fbd.SelectedPath);

                    PrintLog("작업 시작");
                    Task task = new Task(() =>
                    {
                        int co = 0;
                        int wr = 0;
                        foreach (var file in files)
                        {
                            if ((Path.GetExtension(file) != ".jpg") && (Path.GetExtension(file) != ".png"))
                                continue;

                            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
                            sw.Start();

                            Bitmap temp = new Bitmap(file);
                            var label = Path.GetFileName(file);

                            OpenCvSharp.Mat scr = OpenCvSharp.Extensions.BitmapConverter.ToMat(temp);
                            OpenCvSharp.Mat binary = new OpenCvSharp.Mat();

                            OpenCvSharp.Cv2.CvtColor(scr, binary, OpenCvSharp.ColorConversionCodes.BGR2GRAY);
                            OpenCvSharp.Cv2.Threshold(scr, binary, 80, 255, OpenCvSharp.ThresholdTypes.Binary);

//                             ImageFactory factory = new ImageFactory();
//                             factory.Load(OpenCvSharp.Extensions.BitmapConverter.ToBitmap(binary));
//                             factory.GaussianBlur(7);
//                             factory.Contrast(100);
//                             factory.GaussianSharpen(20);
//                             binary = OpenCvSharp.Extensions.BitmapConverter.ToMat((Bitmap)factory.Image);
//                             OpenCvSharp.Cv2.FastNlMeansDenoising(binary, binary, 74, 6, 22);

                            List<Image> listRet = ModelTest.GetSplitImg(OpenCvSharp.Extensions.BitmapConverter.ToBitmap(binary));
                            var ret = ModelTest.AnalyzeImg(listRet);
                            string fullString = string.Join("", ret.ToArray());

                            sw.Stop();

                            if (label.Contains(fullString))
                            {
                                //PrintLog("Correct!");
                                co++;
                            }
                            else
                            {
                                PrintLog($"{label} result : {fullString} -- wrong {sw.ElapsedMilliseconds}");
                                var arrRet = fullString.ToCharArray();
                                var arrLabel = label.ToCharArray();
                                for (int i = 0; i < arrRet.Length; ++i)
                                {
                                    if (arrRet[i] != arrLabel[i])
                                    {
                                        listRet[i].Save($"WrongImg\\{label.Replace(".jpg", "")}_{arrRet[i]}.png");
                                    }
                                }
                                //System.IO.File.Copy(file, $"WrongImg\\{label}");
                                wr++;
                            }

                        }

                        PrintLog($"작업 완료  {co} / {wr}");
                    });
                    task.Start();

                }
            }
        }

        private void button_CheckType_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Images Files(*.jpg; *.jpeg; *.gif; *.bmp; *.png)|*.jpg;*.jpeg;*.gif;*.bmp;*.png";
            DialogResult dr = ofd.ShowDialog();
            if (dr == DialogResult.OK)
            {
                targetImg = new Bitmap(ofd.FileName);
                pictureBox_Type.Image = targetImg;
                Task task = new Task(() => {
                    string ret = ModelTest.AnalyzeImgType(ofd.FileName);
                    label_Type.Invoke(new MethodInvoker(delegate (){
                        label_Type.Text = ret;
                    }));

                    PrintLog("Type 분석 완료");
                });

                PrintLog("Type 분석 시작");
                task.Start();


            }
            else
            {

            }
        }
    }
}
