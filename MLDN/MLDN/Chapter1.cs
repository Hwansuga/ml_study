﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.ML;
using Microsoft.ML.Vision;
using MLDN;
using static Microsoft.ML.DataOperationsCatalog;

public class ImageData
{
    public string ImagePath;

    public string Label;
}

class ModelOutput
{
    public string ImagePath { get; set; }

    public string Label { get; set; }

    public string PredictedLabel { get; set; }
}

class ModelInput
{
    public byte[] Image { get; set; }

    public UInt32 LabelAsKey { get; set; }

    public string ImagePath { get; set; }

    public string Label { get; set; }
}

class Chapter1
{
    public static Form1 uiController = null;

    public static void Do()
    {
        var projectDirectory = Path.GetFullPath(Path.Combine(AppContext.BaseDirectory, "../../../"));
        var workspaceRelativePath = Path.Combine(projectDirectory, "workspace");
        var assetsRelativePath = Path.Combine(projectDirectory, "assets\\NumberImg");
        var exportModelPath = Path.Combine(projectDirectory, "TrainModel\\model.zip");

        MLContext mlContext = new MLContext();

        IEnumerable<ImageData> images = LoadImagesFromDirectory(folder: assetsRelativePath, useFolderNameAsLabel: true);
        IDataView imageData = mlContext.Data.LoadFromEnumerable(images);
        var debug = imageData.Preview();
        IDataView shuffledData = imageData;
        debug = shuffledData.Preview();

        var preprocessingPipeline = mlContext.Transforms.Conversion.MapValueToKey(inputColumnName: "Label",outputColumnName: "LabelAsKey")
                                .Append(mlContext.Transforms.LoadRawImageBytes(outputColumnName: "Image",imageFolder: assetsRelativePath, inputColumnName: "ImagePath"));

        IDataView preProcessedData = preprocessingPipeline.Fit(shuffledData).Transform(shuffledData);
        TrainTestData trainSplit = mlContext.Data.TrainTestSplit(data: preProcessedData, testFraction: 0.3);
        TrainTestData validationTestSplit = mlContext.Data.TrainTestSplit(trainSplit.TestSet);

        IDataView trainSet = trainSplit.TrainSet;
        IDataView validationSet = validationTestSplit.TrainSet;
        IDataView testSet = validationTestSplit.TestSet;
        debug = testSet.Preview();

        var classifierOptions = new ImageClassificationTrainer.Options()
        {
            FeatureColumnName = "Image",
            LabelColumnName = "LabelAsKey",
            ValidationSet = validationSet,
            Arch = ImageClassificationTrainer.Architecture.ResnetV2101,
            MetricsCallback = (metrics) => Console.WriteLine(metrics),
            TestOnTrainSet = false,
            ReuseTrainSetBottleneckCachedValues = false,
            ReuseValidationSetBottleneckCachedValues = false,
            WorkspacePath = workspaceRelativePath
        };

        
        var trainingPipeline = mlContext.MulticlassClassification.Trainers.ImageClassification(classifierOptions)
                            .Append(mlContext.Transforms.Conversion.MapKeyToValue("PredictedLabel"));

        var emtp = trainSet.Preview();

        ITransformer trainedModel = trainingPipeline.Fit(trainSet);
        ClassifySingleImage(mlContext, testSet, trainedModel);
        ClassifyImages(mlContext, testSet, trainedModel);

        mlContext.Model.Save(trainedModel, trainSet.Schema, exportModelPath);
        uiController.PrintLog($"{exportModelPath} 저장 완료");
    }

    private static void OutputPrediction(ModelOutput prediction)
    {
        string imageName = Path.GetFileName(prediction.ImagePath);
        uiController.PrintLog($"Image: {imageName} | Actual Value: {prediction.Label} | Predicted Value: {prediction.PredictedLabel}");
    }

    public static void ClassifySingleImage(MLContext mlContext, IDataView data, ITransformer trainedModel)
    {
        PredictionEngine<ModelInput, ModelOutput> predictionEngine = mlContext.Model.CreatePredictionEngine<ModelInput, ModelOutput>(trainedModel);
        ModelInput image = mlContext.Data.CreateEnumerable<ModelInput>(data, reuseRowObject: true).First();
        ModelOutput prediction = predictionEngine.Predict(image);

        uiController.PrintLog("Classifying single image");
        OutputPrediction(prediction);
    }

    public static void ClassifyImages(MLContext mlContext, IDataView data, ITransformer trainedModel)
    {
        IDataView predictionData = trainedModel.Transform(data);
        IEnumerable<ModelOutput> predictions = mlContext.Data.CreateEnumerable<ModelOutput>(predictionData, reuseRowObject: true).Take(10);
        uiController.PrintLog("Classifying multiple images");
        foreach (var prediction in predictions)
        {
            OutputPrediction(prediction);
        }
        
    }

    public static IEnumerable<ImageData> LoadImagesFromDirectory(string folder, bool useFolderNameAsLabel = true)
    {
        var files = Directory.GetFiles(folder, "*",searchOption: SearchOption.AllDirectories);
        foreach (var file in files)
        {
            if ((Path.GetExtension(file) != ".jpg") && (Path.GetExtension(file) != ".png"))
                continue;

            var label = Path.GetFileName(file);

            if (useFolderNameAsLabel)
                label = Directory.GetParent(file).Name;
            else
            {
                for (int index = 0; index < label.Length; index++)
                {
                    if (!char.IsLetter(label[index]))
                    {
                        label = label.Substring(0, index);
                        break;
                    }

                    

                    
                }
            }
            //uiController.PrintLog($"path : {file} label : {label}");
            yield return new ImageData()
            {
                ImagePath = file,
                Label = label
            };
        }
    }
}

