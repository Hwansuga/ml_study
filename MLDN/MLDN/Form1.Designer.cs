﻿namespace MLDN
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel_Capter = new System.Windows.Forms.Panel();
            this.button_Threshold = new System.Windows.Forms.Button();
            this.button_Split_Save_Path = new System.Windows.Forms.Button();
            this.button_AnalyzeAll = new System.Windows.Forms.Button();
            this.button_split = new System.Windows.Forms.Button();
            this.button_Analyze = new System.Windows.Forms.Button();
            this.button_ModelTest = new System.Windows.Forms.Button();
            this.button_CaptureImage = new System.Windows.Forms.Button();
            this.button_Capter1 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.listBox_log = new System.Windows.Forms.ListBox();
            this.pictureBox_LoadImg = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.label_Result = new System.Windows.Forms.Label();
            this.pictureBox_Type = new System.Windows.Forms.PictureBox();
            this.label_Type = new System.Windows.Forms.Label();
            this.button_CheckType = new System.Windows.Forms.Button();
            this.panel_Capter.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_LoadImg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Type)).BeginInit();
            this.SuspendLayout();
            // 
            // panel_Capter
            // 
            this.panel_Capter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.panel_Capter.Controls.Add(this.button_CheckType);
            this.panel_Capter.Controls.Add(this.button_Threshold);
            this.panel_Capter.Controls.Add(this.button_Split_Save_Path);
            this.panel_Capter.Controls.Add(this.button_AnalyzeAll);
            this.panel_Capter.Controls.Add(this.button_split);
            this.panel_Capter.Controls.Add(this.button_Analyze);
            this.panel_Capter.Controls.Add(this.button_ModelTest);
            this.panel_Capter.Controls.Add(this.button_CaptureImage);
            this.panel_Capter.Controls.Add(this.button_Capter1);
            this.panel_Capter.Controls.Add(this.panel1);
            this.panel_Capter.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_Capter.Location = new System.Drawing.Point(0, 0);
            this.panel_Capter.Name = "panel_Capter";
            this.panel_Capter.Size = new System.Drawing.Size(122, 450);
            this.panel_Capter.TabIndex = 0;
            // 
            // button_Threshold
            // 
            this.button_Threshold.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Threshold.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Threshold.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.button_Threshold.Location = new System.Drawing.Point(0, 193);
            this.button_Threshold.Name = "button_Threshold";
            this.button_Threshold.Size = new System.Drawing.Size(122, 23);
            this.button_Threshold.TabIndex = 8;
            this.button_Threshold.Text = "Analyze_Threshold";
            this.button_Threshold.UseVisualStyleBackColor = true;
            this.button_Threshold.Click += new System.EventHandler(this.button_Threshold_Click);
            // 
            // button_Split_Save_Path
            // 
            this.button_Split_Save_Path.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Split_Save_Path.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Split_Save_Path.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.button_Split_Save_Path.Location = new System.Drawing.Point(0, 170);
            this.button_Split_Save_Path.Name = "button_Split_Save_Path";
            this.button_Split_Save_Path.Size = new System.Drawing.Size(122, 23);
            this.button_Split_Save_Path.TabIndex = 7;
            this.button_Split_Save_Path.Text = "Split_Save_Path";
            this.button_Split_Save_Path.UseVisualStyleBackColor = true;
            this.button_Split_Save_Path.Click += new System.EventHandler(this.button_Split_Save_Path_Click);
            // 
            // button_AnalyzeAll
            // 
            this.button_AnalyzeAll.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_AnalyzeAll.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_AnalyzeAll.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.button_AnalyzeAll.Location = new System.Drawing.Point(0, 147);
            this.button_AnalyzeAll.Name = "button_AnalyzeAll";
            this.button_AnalyzeAll.Size = new System.Drawing.Size(122, 23);
            this.button_AnalyzeAll.TabIndex = 6;
            this.button_AnalyzeAll.Text = "AnalyzeAll";
            this.button_AnalyzeAll.UseVisualStyleBackColor = true;
            this.button_AnalyzeAll.Click += new System.EventHandler(this.button_AnalyzeAll_Click);
            // 
            // button_split
            // 
            this.button_split.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_split.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_split.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.button_split.Location = new System.Drawing.Point(0, 124);
            this.button_split.Name = "button_split";
            this.button_split.Size = new System.Drawing.Size(122, 23);
            this.button_split.TabIndex = 5;
            this.button_split.Text = "Split_Img";
            this.button_split.UseVisualStyleBackColor = true;
            this.button_split.Click += new System.EventHandler(this.button_split_Click);
            // 
            // button_Analyze
            // 
            this.button_Analyze.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Analyze.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Analyze.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.button_Analyze.Location = new System.Drawing.Point(0, 101);
            this.button_Analyze.Name = "button_Analyze";
            this.button_Analyze.Size = new System.Drawing.Size(122, 23);
            this.button_Analyze.TabIndex = 4;
            this.button_Analyze.Text = "Analyze";
            this.button_Analyze.UseVisualStyleBackColor = true;
            this.button_Analyze.Click += new System.EventHandler(this.button_Analyze_Click);
            // 
            // button_ModelTest
            // 
            this.button_ModelTest.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_ModelTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_ModelTest.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.button_ModelTest.Location = new System.Drawing.Point(0, 78);
            this.button_ModelTest.Name = "button_ModelTest";
            this.button_ModelTest.Size = new System.Drawing.Size(122, 23);
            this.button_ModelTest.TabIndex = 3;
            this.button_ModelTest.Text = "ModelTest";
            this.button_ModelTest.UseVisualStyleBackColor = true;
            this.button_ModelTest.Click += new System.EventHandler(this.button_ModelTest_Click);
            // 
            // button_CaptureImage
            // 
            this.button_CaptureImage.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_CaptureImage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_CaptureImage.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.button_CaptureImage.Location = new System.Drawing.Point(0, 55);
            this.button_CaptureImage.Name = "button_CaptureImage";
            this.button_CaptureImage.Size = new System.Drawing.Size(122, 23);
            this.button_CaptureImage.TabIndex = 2;
            this.button_CaptureImage.Text = "CaptureImage";
            this.button_CaptureImage.UseVisualStyleBackColor = true;
            this.button_CaptureImage.Click += new System.EventHandler(this.button_CaptureImage_Click);
            // 
            // button_Capter1
            // 
            this.button_Capter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Capter1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Capter1.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.button_Capter1.Location = new System.Drawing.Point(0, 32);
            this.button_Capter1.Name = "button_Capter1";
            this.button_Capter1.Size = new System.Drawing.Size(122, 23);
            this.button_Capter1.TabIndex = 1;
            this.button_Capter1.Text = "Capter1";
            this.button_Capter1.UseVisualStyleBackColor = true;
            this.button_Capter1.Click += new System.EventHandler(this.button_Capter1_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(122, 32);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.listBox_log);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel2.Location = new System.Drawing.Point(404, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(500, 450);
            this.panel2.TabIndex = 1;
            // 
            // listBox_log
            // 
            this.listBox_log.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBox_log.FormattingEnabled = true;
            this.listBox_log.ItemHeight = 12;
            this.listBox_log.Location = new System.Drawing.Point(0, 0);
            this.listBox_log.Name = "listBox_log";
            this.listBox_log.Size = new System.Drawing.Size(500, 450);
            this.listBox_log.TabIndex = 0;
            // 
            // pictureBox_LoadImg
            // 
            this.pictureBox_LoadImg.Location = new System.Drawing.Point(128, 5);
            this.pictureBox_LoadImg.Name = "pictureBox_LoadImg";
            this.pictureBox_LoadImg.Size = new System.Drawing.Size(110, 50);
            this.pictureBox_LoadImg.TabIndex = 2;
            this.pictureBox_LoadImg.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(128, 61);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(40, 50);
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(180, 61);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(40, 50);
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Location = new System.Drawing.Point(234, 61);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(40, 50);
            this.pictureBox3.TabIndex = 5;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Location = new System.Drawing.Point(299, 61);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(40, 50);
            this.pictureBox4.TabIndex = 6;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Location = new System.Drawing.Point(353, 61);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(40, 50);
            this.pictureBox5.TabIndex = 7;
            this.pictureBox5.TabStop = false;
            // 
            // label_Result
            // 
            this.label_Result.AutoSize = true;
            this.label_Result.Location = new System.Drawing.Point(128, 124);
            this.label_Result.Name = "label_Result";
            this.label_Result.Size = new System.Drawing.Size(38, 12);
            this.label_Result.TabIndex = 8;
            this.label_Result.Text = "label1";
            // 
            // pictureBox_Type
            // 
            this.pictureBox_Type.Location = new System.Drawing.Point(130, 388);
            this.pictureBox_Type.Name = "pictureBox_Type";
            this.pictureBox_Type.Size = new System.Drawing.Size(110, 50);
            this.pictureBox_Type.TabIndex = 9;
            this.pictureBox_Type.TabStop = false;
            // 
            // label_Type
            // 
            this.label_Type.AutoSize = true;
            this.label_Type.Location = new System.Drawing.Point(261, 426);
            this.label_Type.Name = "label_Type";
            this.label_Type.Size = new System.Drawing.Size(38, 12);
            this.label_Type.TabIndex = 10;
            this.label_Type.Text = "label1";
            // 
            // button_CheckType
            // 
            this.button_CheckType.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_CheckType.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_CheckType.ForeColor = System.Drawing.SystemColors.ActiveCaption;
            this.button_CheckType.Location = new System.Drawing.Point(0, 216);
            this.button_CheckType.Name = "button_CheckType";
            this.button_CheckType.Size = new System.Drawing.Size(122, 23);
            this.button_CheckType.TabIndex = 9;
            this.button_CheckType.Text = "CheckType";
            this.button_CheckType.UseVisualStyleBackColor = true;
            this.button_CheckType.Click += new System.EventHandler(this.button_CheckType_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(904, 450);
            this.Controls.Add(this.label_Type);
            this.Controls.Add(this.pictureBox_Type);
            this.Controls.Add(this.label_Result);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox_LoadImg);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel_Capter);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.Text = "MLDN";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel_Capter.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_LoadImg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_Type)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel_Capter;
        private System.Windows.Forms.Button button_Capter1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListBox listBox_log;
        private System.Windows.Forms.Button button_CaptureImage;
        private System.Windows.Forms.Button button_ModelTest;
        private System.Windows.Forms.PictureBox pictureBox_LoadImg;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Button button_Analyze;
        private System.Windows.Forms.Label label_Result;
        private System.Windows.Forms.Button button_split;
        private System.Windows.Forms.Button button_AnalyzeAll;
        private System.Windows.Forms.Button button_Split_Save_Path;
        private System.Windows.Forms.Button button_Threshold;
        private System.Windows.Forms.PictureBox pictureBox_Type;
        private System.Windows.Forms.Label label_Type;
        private System.Windows.Forms.Button button_CheckType;
    }
}

